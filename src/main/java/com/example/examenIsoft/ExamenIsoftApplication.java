package com.example.examenIsoft;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExamenIsoftApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExamenIsoftApplication.class, args);
	}

}
